import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _biggerFont = TextStyle(
    fontSize: 18.0,
  );
  List<Widget> widgets = [];

  _MyAppState() {
    for (int i = 1; i <= 100; i++)
      widgets.add(Container(
        padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
        child: i % 3 == 0 && i % 5 == 0
            ? Text(
                'ApaBole',
                style: _biggerFont,
              )
            : i % 3 == 0
                ? Text(
                    'Apa',
                    style: _biggerFont,
                  )
                : i % 5 == 0
                    ? Text(
                        'Bole',
                        style: _biggerFont,
                      )
                    : Text(
                        '$i',
                        style: _biggerFont,
                      ),
      ));
    // if (i % 3 == 0 && i % 5 == 0) {
    //   widgets.add(Container(
    //       padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
    //       child: Text('Apa Bole', style: _biggerFont)));
    // } else if (i % 3 == 0) {
    //   widgets.add(Container(
    //       padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
    //       child: Text('Apa', style: _biggerFont)));
    // } else if (i % 5 == 0) {
    //   widgets.add(Container(
    //       padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
    //       child: Text('Bole', style: _biggerFont)));
    // } else {
    //   widgets.add(Container(
    //       padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
    //       child: Text('$i', style: _biggerFont)));
    // }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text('apaBole Apps')),
      body: ListView(
        padding: EdgeInsets.all(16.0),
        children: <Widget>[
          Text(
            "This apps will show you number 1 - 100, but when it can divided by 3 it printed 'Apa', when it can divided by 5 printed 'Bole', when it can divided by 3 and 5 it printed 'ApaBole'",
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: widgets,
          )
        ],
      ),
    ));
  }
}
